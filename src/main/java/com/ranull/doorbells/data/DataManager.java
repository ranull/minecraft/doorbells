package com.ranull.doorbells.data;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.ranull.doorbells.Doorbells;

public class DataManager {
	private Doorbells plugin;
	private FileConfiguration data;
	private File dataFile;

	public DataManager(Doorbells plugin) {
		this.plugin = plugin;
	}

	public void loadData() throws IOException, InvalidConfigurationException {
		data.load(dataFile);
	}

	public FileConfiguration getData() {
		return this.data;
	}

	public void saveDoorbell(Location loc) {
		String block = loc.getBlock().getType().toString();
		String world = loc.getWorld().getName();
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		data.set(world + "." + x + "_" + y + "_" + z, block);
		saveData();
	}

	public boolean checkDoorbell(Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		String doorbell = data.getString(world + "." + x + "_" + y + "_" + z);
        return doorbell != null;
    }

	public void removeDoorbell(Location loc) {
		String world = loc.getWorld().getName();
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		data.set(world + "." + x + "_" + y + "_" + z, null);
		saveData();
	}

	public void saveData() {
		try {
			data.save(dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createDataFile() {
		dataFile = new File(plugin.getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			try {
				dataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	public List<String> getMaterials() {
		List<String> material = plugin.getConfig().getStringList("settings.material");
		return material;
	}

}
