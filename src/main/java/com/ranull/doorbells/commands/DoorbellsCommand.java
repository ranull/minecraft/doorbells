package com.ranull.doorbells.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.ranull.doorbells.Doorbells;

public class DoorbellsCommand implements CommandExecutor {
	private Doorbells plugin;

	public DoorbellsCommand(Doorbells plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String version = "1.1";
		String author = "Ranull";

		if (args.length < 1) {
			sender.sendMessage(
					ChatColor.DARK_GRAY + "» " + ChatColor.AQUA + "Doorbells " + ChatColor.GRAY + "v" + version);
			sender.sendMessage(
					ChatColor.GRAY + "/doorbells " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");
			if (sender.hasPermission("doorbells.reload")) {
				sender.sendMessage(ChatColor.GRAY + "/doorbells reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
						+ " Reload plugin");
			}
			sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
			return true;
		}
		if (args.length == 1 && args[0].equals("reload")) {
			if (!sender.hasPermission("doorbells.reload")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "Doorbells" + ChatColor.DARK_GRAY + "]"
						+ ChatColor.RESET + " No Permission!");
				return true;
			}
			plugin.reloadConfig();
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "Doorbells" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " Reloaded config file!");
		}
		return true;
	}
}
